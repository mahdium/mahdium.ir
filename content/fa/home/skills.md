+++
# A Skills section created with the Featurette widget.
widget = "bars"  # See https://sourcethemes.com/academic/docs/page-builder/
weight = 20  # Order that this section will appear.

# Uncomment the following line and widget will NOT be displayed
 hidden = true

title = "مهارت ها"
subtitle = "!عاشق فضا و برنامه نویسی، همیشه دنبال یادگیری چیزای جدید"

[[bar]]
	icon = "images/icons/linux.webp"
	name = "Linux"
	percent = "35%"

[[bar]]
	#icon = "/icons/heat.svg"
	name = "Soon"
	percent = "50%"

+++
