+++
widget = "timeline"
weight = 30  # Order that this section will appear.

# Uncomment the following line and widget will NOT be displayed
# hidden = true

# Widget title
title = "پروژه ها"
# Widget subtitle
subtitle = ""

date_format = "Jan 2006" # Date format https://gohugo.io/functions/dateformat/#readout

[[period]]
  title = "پنل هوشمند حضوروغیاب"
  subtitle = "کنترل حضور و غیاب"
  location = "[مشاهده وبسایت](https://group7sky.ir)" 
  date_start = "2023-08-01"
  date_end = ""
  description = "کنترل حضور و غیاب در مدارس با پنل مدیریتی پیشرفته. (طراح و برنامه نویس بخش سخت افزار و مدیر سرور)"

[[period]]
title = "نیم بها کننده لینک دانلود مُنَصِف"
subtitle = "همون فایل، نصف هزینه اینترنت ⁦;⁠-⁠)"
location = "[مشاهده وبسایت](https://monasef.ir)"
date_start = "2023-11-05"
date_end = ""
description = "هر فایلی رو نیم بها دانلود کن!"

[[period]]
title = "Arsh Gallery"
subtitle = "گالری عرش"
location = "[مشاهده وبسایت](https://arsh.monasef.ir)"
date_start = "2023-09-18"
date_end = ""
description = "نمونه کارهای امین عرشیا مومنی"

[[period]]
title = "Cats Of Mastodon telegram bot"
subtitle = ""
location = "[مشاهده در تلگرام](https://t.me/CatsOfMastodonMODbot)"
date_start = "2024-09-14"
date_end = ""
description = "A simple scraper for mastodon public timeline, gathering cat photos and sending them to a channel!"
+++