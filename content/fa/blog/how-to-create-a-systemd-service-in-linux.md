+++
title= "چگونه یک سرویس Systemd در لینوکس ایجاد کنیم"
date= 2023-09-28T15:39:06+03:30
draft= false
tags = ['Systemd']
description = "نحوه ایجاد یک سرویس Systemd در لینوکس"
banner = "images/how-to-create-a-systemd-service-in-linux/fa.webp"
+++
![چگونه یک سرویس Systemd در لینوکس ایجاد کنیم](images/how-to-create-a-systemd-service-in-linux/fa.webp "نحوه ایجاد یک سرویس Systemd در لینوکس")

## **Systemd** یک سیستم init است که در بسیاری از توزیع‌های لینوکس از جمله CentOS، Ubuntu و Fedora استفاده می‌شود.
 ## **systemd** مسئول مدیریت سرویس‌ها، سخت‌افزار، mount point ها و سایر موجودیت‌های سیستم عامل است.

گاهی اوقات شما یک اسکریپت ایجاد می‌کنید و سپس می‌خواهید اسکریپت‌ها توسط **systemd** کنترل شوند یا در برخی موارد می‌خواهید که اسکریپت‌ها به خودی خود مجدداً راه‌اندازی شوند که به دلایلی متوقف شوند. در چنین مواردی **systemd** در لینوکس به پیکربندی سرویس هایی که می توانند مدیریت شوند کمک می کند. برای این کار مراحل زیر را دنبال کنید.

برای ایجاد یک سرویس **Systemd**، باید یک فایل پیکربندی ایجاد کنید که اطلاعات مربوط به سرویس را مشخص می‌کند. این فایل‌ها با پسوند .service در یکی از سه مسیر زیر قرار می‌گیرند:
```
/usr/lib/systemd/system
/usr/lib/systemd/user
/etc/systemd/system
```

**1-** برای ساخت یک سرویس جدید در ابتدا دستور زیر را وارد میکنیم تا دایرکتوری محل کار ما تغییر کند
```bash
cd /etc/systemd/system
```
**2-** فایلی به نام your-service.service ایجاد کنید و موارد زیر را در آن قرار دهید:

```systemd
[Unit]
Description=<description about this service>

[Service]
User=<user e.g. root>
WorkingDirectory=<directory_of_script e.g. /root>
ExecStart=<script which needs to be executed>
# optional items below
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```

یا برای یک برنامه پایتون به این صورت عمل میکنیم:
```systemd
[Unit]
Description=<project description>

[Service]
User=<user e.g. root>
WorkingDirectory=<path to your project directory>
ExecStart=/bin/bash -c 'cd /home/ubuntu/project/ && source venv/bin/activate && python test.py'
# optional items below
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```

**3-** فایل های سرویس را مجدداً بارگیری کنید تا سرویس جدید اضافه شود.
```bash
sudo systemctl daemon-reload
```

برای فعال کردن سرویس (اجرا شدن خودکار پس از ریستارت سیستم)، باید دستور زیر را اجرا کنید:
```bash
sudo systemctl enable your-service.service
```
برای راه‌اندازی سرویس، باید دستور زیر را اجرا کنید:
```bash
sudo systemctl start your-service.service
```
برای بررسی وضعیت سرویس، می‌توانید دستور زیر را اجرا کنید:
```bash
sudo systemctl status your-service.service
```

{{< clicky >}}
