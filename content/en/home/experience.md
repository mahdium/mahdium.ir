+++
widget = "timeline"
weight = 30  # Order that this section will appear.

# Uncomment the following line and widget will NOT be displayed
# hidden = true

# Widget title
title = "Experience"
# Widget subtitle
subtitle = "My projects timeline"

date_format = "Jan 2006" # Date format https://gohugo.io/functions/dateformat/#readout

[[period]]
   title = "Presence and Absence Smart Panel"
   subtitle = "Attendance control"
   location = "[View website](https://group7sky.ir)"
   date_start = "2023-08-01"
   date_end = ""
   description = "Attendance control in schools with an advanced control panel. (Hardware department designer and programmer - Server manager)"

[[period]]
title = "Half-priced fair download link (For Iran Only)"
subtitle = "The same file, half the cost of the internet ;⁠-⁠)"
location = "[View website](https://monasef.ir)"
date_start = "2023-11-05"
date_end = ""
description = "Download any file at half the internet price!"

[[period]]
title = "Arsh Gallery"
subtitle = "Arsh Gallery"
location = "[View website](https://arsh.monasef.ir)"
date_start = "2023-09-18"
date_end = ""
description = "A gallery for Amin Arshaya Momeni's designs."

[[period]]
title = "TVC Algorithm for hall effect thrusters"
subtitle = "TVC Algorithm"
location = "Personal project (WIP)"
date_start = "2022-11-03"
date_end = ""
description = "A simple TVC algorithm for hall effect thrusters (AU)"

[[period]]
title = "Cats Of Mastodon telegram bot"
subtitle = ""
location = "[View on Telegram](https://t.me/CatsOfMastodonMODbot)"
date_start = "2024-09-14"
date_end = ""
description = "A simple scraper for mastodon public timeline, gathering cat photos and sending them to a channel!"
+++