+++
title= "How to create a Systemd service in Linux"
date= 2023-09-28T15:39:06+03:30
draft= false
tags = ['Systemd']
description = "How to Create a Systemd Service in Linux"
banner = "images/how-to-create-a-systemd-service-in-linux/en.webp"
+++
![How to Create a Systemd Service in Linux](images/how-to-create-a-systemd-service-in-linux/en.webp "How to Create a Systemd Service in Linux")
## **Systemd** is an init system used in many Linux distributions including CentOS, Ubuntu and Fedora.
## **systemd** is responsible for managing services, hardware, mount points and other operating system entities.

Sometimes you create a script and then you want the scripts to be handled by **systemd** or sometimes you want the scripts to restart themselves when they stop for some reason. In such cases **systemd** in Linux helps to configure the services that can be managed. To do this, follow the steps below.

To create a Systemd service, you must create a configuration file that specifies information about the service. These files with the .service extension are placed in one of the following three paths:
```
/usr/lib/systemd/system
/usr/lib/systemd/user
/etc/systemd/system
```

**1-** To create a new service, first enter the following command to change our work directory
```bash
cd /etc/systemd/system
```
**2-** Create a file called your-service.service and put the following in it:

```systemd
[Unit]
Description=<description about this service>

[Service]
User=<user e.g. root>
WorkingDirectory=<directory_of_script e.g. /root>
ExecStart=<script which needs to be executed>
# optional items below
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```

Or for a Python program, we do it like this:
```systemd
[Unit]
Description=<project description>

[Service]
User=<user e.g. root>
WorkingDirectory=<path to your project directory>
ExecStart=/bin/bash -c 'cd /home/ubuntu/project/ && source venv/bin/activate && python test.py'
# optional items below
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```

**3-** Reload the service files to add the new service.
```bash
sudo systemctl daemon-reload
```

To activate the service (automatically running after system restart), you need to run the following command:
```bash
sudo systemctl enable your-service.service
```
To start the service, you need to run the following command:
```bash
sudo systemctl start your-service.service
```
To check the status of the service, you can run the following command:
```bash
sudo systemctl status your-service.service
```
{{< clicky >}}
