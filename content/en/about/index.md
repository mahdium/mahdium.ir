+++
title = "About me"

type = "blog"
+++

## A 17-year-old **student**, who loves space and programming!


### I am very interested in learning and sharing what I have learned with others. 
Of course, I try my best to learn things that help to increase the quality of my life and pass them on to others.


## I Occasionally write about my daily life or even some tutorials and share my experience [**At My Blog!**](https://urls.st/mahdium-en-blog)